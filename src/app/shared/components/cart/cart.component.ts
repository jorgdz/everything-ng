import { Component, OnInit } from '@angular/core';
import { ShopingCartService } from '../../services/shoping-cart.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
})
export class CartComponent implements OnInit {

  quantity$ = this.shoppingCartService.quantityAction$;
  total$ = this.shoppingCartService.totalAction$;
  cart$ = this.shoppingCartService.cartAction$;

  constructor(private shoppingCartService: ShopingCartService) { }

  ngOnInit(): void {
  }

}
