import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {
  formCheckout!: FormGroup;

  stores = [
    {
      "id": 1,
      "name": "Park Row at Beekman St",
      "address": "38 Park Row",
      "city": "New York",
      "openingHours": "10:00 - 14:00 and 17:00 - 20:30"
    },
    {
      "id": 2,
      "name": "Store Alcalá",
      "address": "Calle de Alcalá, 21",
      "city": "Madrid",
      "openingHours": "10:00 - 14:00 and 17:00 - 20:30"
    }
  ];

  constructor(private formBuilder: FormBuilder) {
    this.validateForm();
  }

  ngOnInit(): void { }

  onPickupOrDelivery (value: boolean): void {
    console.log(value)
  }

  sendCheckout () {
    if (this.formCheckout.valid)
      console.log(this.formCheckout.value)
    else
      console.log(this.formCheckout.invalid)
  }

  validateForm () {
    this.formCheckout = this.formBuilder.group({
      name: ['', Validators.required],
      store: ['', Validators.required],
      shippingAddress: [''],
      city: ['', Validators.required],
    });
  }
}
